package postilion.cardtronics.termmigrator;

/**
 * @author iainteale
 */
public class MigrateResponse
{
	private Status status;
	private String message;
	private TermType source;
	private TermType destination;
	private String terminal_id;

	public enum Status {OK, ERROR};
	
	public enum TermType {ATMAPP, POST_TERM, NONE};
	
	public MigrateResponse() {
		
	}
	
	public MigrateResponse(Status status, String message, TermType source, TermType destination, String terminal_id) {
		this.status = status;
		this.message = message;
		this.source = source;
		this.destination = destination;
		this.terminal_id = terminal_id;
	}
	
	public Status getStatus() {
		return this.status;
	}
	
	public String getMessage() {
		return message;
	}
	
	public TermType getSource() {
		return source;
	}

	public TermType getDestination() {
		return destination;
	}
	
	public String getTerminalID() {
		return terminal_id;
	}
	
	public void setTerminalID(String terminal_id) {
		this.terminal_id = terminal_id;
	}
	
	public void setDestination(TermType destination) {
		this.destination = destination;
	}
	
	public void setSource(TermType source) {
		this.source = source;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public void setStatus(Status status) {
		this.status = status;
	}
}