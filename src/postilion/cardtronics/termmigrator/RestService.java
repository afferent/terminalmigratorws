package postilion.cardtronics.termmigrator;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import postilion.cardtronics.termmigrator.MigrateResponse.TermType;

/**
 * @author iainteale
 */
@Path("/migrate")
public class RestService
{
	@GET
	@Path("{server}/{terminal_id}")
	@Produces(MediaType.TEXT_PLAIN)
	public String migrate(@PathParam("server") String server, @PathParam("terminal_id") String terminal_id) throws Exception {
		CommandUtil cmd_util = new CommandUtil();
		MigrateResponse response = cmd_util.sendCommand(server, "TerminalMigratorService", "MIGRATE_" + terminal_id);
		return buildResponse(response);
	}
	
	@GET
	@Path("status/{server}/{terminal_id}")
	@Produces(MediaType.TEXT_PLAIN)
	public String status(@PathParam("server") String server, @PathParam("terminal_id") String terminal_id) throws Exception {
		CommandUtil cmd_util = new CommandUtil();
		MigrateResponse response = cmd_util.sendCommand(server, "TerminalMigratorService", "STATUS_" + terminal_id);
		return buildResponse(response);
	}
	
	@GET
	@Path("migrateToAtmApp/{server}/{terminal_id}")
	@Produces(MediaType.TEXT_PLAIN)
	public String migrateToAtmApp(@PathParam("server") String server, @PathParam("terminal_id") String terminal_id) throws Exception {
		CommandUtil cmd_util = new CommandUtil();
		MigrateResponse response = cmd_util.sendCommand(server, "TerminalMigratorService", "MIGRATETOATMAPP_" + terminal_id);
		return buildResponse(response);
	}
	
	@GET
	@Path("migrateToPostTerm/{server}/{terminal_id}")
	@Produces(MediaType.TEXT_PLAIN)
	public String migrateToPostTerm(@PathParam("server") String server, @PathParam("terminal_id") String terminal_id) throws Exception {
		CommandUtil cmd_util = new CommandUtil();
		MigrateResponse response = cmd_util.sendCommand(server, "TerminalMigratorService", "MIGRATETOPOSTTERM_" + terminal_id);
		return buildResponse(response);
	}
	
	public String buildResponse(MigrateResponse response) {
		StringBuilder builder = new StringBuilder();
		builder.append("Status:").append(response.getStatus());
		builder.append(",Terminal:").append(response.getTerminalID());

		builder.append(",Target:");
		if (response.getDestination() ==  TermType.NONE) {
			builder.append("N/A");
		} else {
			builder.append(response.getDestination());
		}
		
		builder.append(",Source:");
		if (response.getDestination() ==  TermType.NONE) {
			builder.append("N/A");
		} else {
			builder.append(response.getSource());
		}
		
		builder.append(",Message:").append(response.getMessage());
		
		return builder.toString(); 
	}
}
