package postilion.cardtronics.termmigrator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Serializer to serialize Java objects to JSON using Google's
 * GSON libraries.
 * 
 * @author iteale
 */
public class GsonSerializer
{
	private final Gson gson;
	private static final GsonSerializer instance = new GsonSerializer();
	
	public GsonSerializer()
	{
		gson = new GsonBuilder().disableHtmlEscaping().create();
	}

	public String asString(Object obj) throws Exception
	{
		return gson.toJson(obj);
	}
	
	public <T> T asObject(String json, Class<T> classOfT) throws Exception {
		return gson.fromJson(json, classOfT);
	}
	
	public static final GsonSerializer getInstance() {
		return instance;
	}
}