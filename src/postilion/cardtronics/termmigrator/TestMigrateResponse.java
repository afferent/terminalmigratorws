/**
 * 
 */
package postilion.cardtronics.termmigrator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import postilion.cardtronics.termmigrator.MigrateResponse.Status;
import postilion.cardtronics.termmigrator.MigrateResponse.TermType;

/**
 * @author iainteale
 */
public class TestMigrateResponse
{
	/**
	 * Test method for {@link postilion.cardtronics.termmigrator.MigrateResponse#MigrateResponse(postilion.cardtronics.termmigrator.MigrateResponse.Status, java.lang.String, postilion.cardtronics.termmigrator.MigrateResponse.TermType, postilion.cardtronics.termmigrator.MigrateResponse.TermType)}.
	 * @throws Exception 
	 */
	@Test
	public final void testSerializeMigrateResponse() throws Exception
	{
		GsonSerializer serializer = new GsonSerializer();
		String expected = "{\"status\":\"OK\",\"message\":\"Message\",\"source\":\"ATMAPP\",\"destination\":\"POST_TERM\",\"terminal_id\":\"00000001\"}";
		assertEquals(expected, serializer.asString(new MigrateResponse(Status.OK, "Message", TermType.ATMAPP, TermType.POST_TERM, "00000001")));
	}
	
	/**
	 * Test method for {@link postilion.cardtronics.termmigrator.MigrateResponse#MigrateResponse(postilion.cardtronics.termmigrator.MigrateResponse.Status, java.lang.String, postilion.cardtronics.termmigrator.MigrateResponse.TermType, postilion.cardtronics.termmigrator.MigrateResponse.TermType)}.
	 * @throws Exception 
	 */
	@Test
	public final void testMigrateResponse() throws Exception
	{
		MigrateResponse mr = new MigrateResponse(Status.OK, "Message", TermType.ATMAPP, TermType.POST_TERM,"00000001");

		RestService r = new RestService();
		
		String expected = "Status:OK,Terminal:00000001,Target:POST_TERM,Source:ATMAPP,Message:Message";
		String actual = r.buildResponse(mr);
		assertEquals(expected, actual);
	}
}