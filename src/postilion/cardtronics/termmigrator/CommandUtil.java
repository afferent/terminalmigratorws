package postilion.cardtronics.termmigrator;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import postilion.cardtronics.termmigrator.MigrateResponse.Status;
import postilion.realtime.sdk.ipc.IClientEndpoint;
import postilion.realtime.sdk.ipc.Sap;
import postilion.realtime.sdk.ipc.TcpClientSap;
import postilion.realtime.sdk.util.Queue;
import postilion.realtime.sdk.util.Timer;

public class CommandUtil
{
	private TcpClientSap	sap;
	private IClientEndpoint	endpoint;
	private Timer			timer;

	private int				result	= CommandResult.OK;

	private Queue			queue	= new Queue();

	/*----------------------------------------------------------------------------*/
	public synchronized MigrateResponse sendCommand(String host, String task, String command) throws Exception
	{
		String rsp = null;
		MigrateResponse response = new MigrateResponse();

		//
		//Put the command into the database, using SDK command utility.
		//Returns the "PROCESS_<id>" command, with the command ID.
		String process_cmd = addCommandToDB(host, task, command);

		//
		//Make the connection and start the Sap
		sap = new TcpClientSap("CommandUtil", this.queue, null);
		sap.start();

		if (connectToCommandPort(host, task))
		{
			this.timer = new Timer(this.queue, TIMEOUT, TIMER_LABEL_RESPONSE);
			this.endpoint.send(process_cmd);

			rsp = waitForResponse();
			if (rsp != null)
			{
				response = GsonSerializer.getInstance().asObject(rsp, MigrateResponse.class);
			}
		}

		switch (this.result)
		{
		case CommandResult.NO_SUCH_TASK :
			response.setMessage("Error retrieving the command port for the service from the host");
			response.setStatus(Status.ERROR);
			break;
		case CommandResult.DISCONNECTED :
			response.setMessage("Error retrieving the command port from the remote host");
			response.setStatus(Status.ERROR);
			break;
		case CommandResult.TIMEOUT_CONNECTING :
			response.setMessage("Error connecting to the command port on the remote host");
			response.setStatus(Status.ERROR);
			break;
		case CommandResult.TIMEOUT_NO_RESPONSE :
			response.setMessage("No response was received from the remote host");
			response.setStatus(Status.ERROR);
			break;
		case CommandResult.OK :
		default :
			response.setMessage("OK");
			response.setStatus(Status.OK);
			break;
		}

		return response;
	}

	/*----------------------------------------------------------------------------*/
	/**
	 * Block until a response or an error event (timeout or disconnect) appears on the queue.
	 */
	protected String waitForResponse()
	{
		Object event = this.queue.next();

		if (event instanceof Sap.DisconnectEvent)
		{
			done(CommandResult.DISCONNECTED);
		}
		else if (event instanceof Sap.DataEvent)
		{
			done(CommandResult.OK);
			return new String(((Sap.DataEvent)event).data);
		}
		else if (event instanceof Timer.Event)
		{
			if (TIMER_LABEL_CONNECT.equals(((Timer.Event)event).user_ref))
			{
				//Ignore leftover connect timeout on the queue. Already connected.
				return waitForResponse();
			}
			else //if (TIMER_LABEL_RESPONSE.equals(((Timer.Event)event).user_ref))
			{
				done(CommandResult.TIMEOUT_NO_RESPONSE);
			}
		}

		return null;
	}

	/*-------------------------------------------------------------------------*/
	/**
	 * Connect to the command port for the specified task
	 * 
	 * @return true if connected successfully; false otherwise
	 * @throws SQLException
	 *             if there is a database error looking up the command port for the task
	 */
	protected boolean connectToCommandPort(String remote_host, String task) throws SQLException
	{
		String host = null;
		int port = 0;

		//retrieve the host address and port from the task table

		Connection conn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;

		try
		{
			conn = getDbConnection(remote_host);
			conn.setAutoCommit(true);
			
			stmt = conn.prepareCall("{call task_commandport_get (?)}");
			stmt.setString(1, task);
			
			rs = stmt.executeQuery();

			while (rs.next())
			{
				host = rs.getString(1);
				port = rs.getInt(2);
			}
		}
		finally
		{
			cleanup(conn, stmt, rs);
		}

		if (port == 0)
		{
			done(CommandResult.NO_SUCH_TASK);
			return false;
		}

		this.timer = new Timer(this.queue, TIMEOUT, TIMER_LABEL_CONNECT);
		endpoint = sap.getEndpoint(host, port);
		endpoint.connect();

		Object next = this.queue.next();

		if (next instanceof Sap.ConnectEvent)
		{
			this.timer.stop();
			return true;
		}
		else if (next instanceof Sap.DisconnectEvent)
		{
			done(CommandResult.DISCONNECTED);
			return false;
		}
		else //only other option is Timer.Event, but make it a catch-all 
		{
			done(CommandResult.TIMEOUT_CONNECTING);
			return false;
		}
	}

	/*-------------------------------------------------------------------------*/
	/**
	 * All done. Cleanup everything, including stopping our own processor, and unblock the waiting thread.
	 */
	protected void done(int result)
	{
		this.queue.close();

		if (this.timer != null)
		{
			this.timer.stop();
		}
		if (this.endpoint != null)
		{
			this.endpoint.close();
		}

		if (this.sap != null)
		{
			this.sap.close();
		}

		setResult(result);
	}

	/*-------------------------------------------------------------------------*/
	protected void setResult(int result)
	{
		this.result = result;
	}

	/*-------------------------------------------------------------------------*/
	protected int getResult()
	{
		return this.result;
	}

	/*-------------------------------------------------------------------------*/

	/**
	 * <p>
	 * Adds a command to tm_command_requests and returns the command in the format 'PROCESS_x' where x is the next value
	 * of the identity column tm_command_requests.command_id.
	 * </p>
	 * 
	 * @param app_name
	 *            The name of the application to which the command should be sent.
	 * @param command
	 *            The command to be sent.
	 * @exception SQLException
	 *                Thrown when the current SQL user does not have execute rights on the stored procedure
	 *                tm_command_requests_add.
	 */
	private String addCommandToDB(String host, String app_name, String command) throws SQLException
	{
		Connection cn = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		long command_id = 0;
		String host_name, host_dotted_ip;
		InetAddress host_ip;

		try
		{
			host_name = InetAddress.getLocalHost().getHostName();
			host_ip = InetAddress.getByName(host_name);
			host_dotted_ip = host_ip.getHostAddress();
		}
		catch(UnknownHostException e)
		{
			//Cannot audit this command if the host name cannot be determined. 
			return null;
		}

		try
		{
			cn = getDbConnection(host);
			cn.setAutoCommit(true);
			stmt = cn.prepareCall("{call tm_command_requests_add (?,?,?)}");
			stmt.setString(1, app_name);
			stmt.setString(2, host_dotted_ip);
			stmt.setString(3, command);
			rs = stmt.executeQuery();

			if (rs.next())
			{
				command_id = rs.getLong(1);
			}
		}
		finally
		{
			cleanup(cn, stmt, rs);
		}

		if (command_id == 0)
		{
			return null;
		}

		return "PROCESS_" + Long.toString(command_id);
	}

	/*-------------------------------------------------------------------------*/

	private Connection getDbConnection(String host) throws SQLException
	{
		return DriverManager.getConnection(
				"jdbc:jtds:sqlserver://" + host + "/realtime;sendStringParametersAsUnicode=false;appName=MigrateTerminalWS;progName=MigrateTerminalWS;");
	}

	/*-------------------------------------------------------------------------*/

	private void cleanup(Connection cn, Statement stmt, ResultSet rs) throws SQLException
	{
		if (rs != null) {
			rs.clearWarnings();
			rs.close();
		}
		
		if (stmt != null) {
			stmt.clearWarnings();
			stmt.close();
		}
		
		if (cn != null) {
			cn.clearWarnings();
			cn.close();
		}
	}

	/*-------------------------------------------------------------------------*/
	/* static 
	/*-------------------------------------------------------------------------*/

	private static final int	TIMEOUT					= 10 * 1000;		// 10 seconds for connect or rsp

	private static final String	TIMER_LABEL_CONNECT		= "Connect timer";
	private static final String	TIMER_LABEL_RESPONSE	= "Response timer";

	/*-------------------------------------------------------------------------*/
	/* static inner class
	/*-------------------------------------------------------------------------*/

	protected static class CommandResult
	{
		public static final int	OK					= 0;
		public static final int	DISCONNECTED		= 1;
		public static final int	TIMEOUT_CONNECTING	= 2;
		public static final int	TIMEOUT_NO_RESPONSE	= 3;
		public static final int	NO_SUCH_TASK		= 4;
	}

	static
	{
		try
		{
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
		}
		catch(ClassNotFoundException e)
		{
			e.printStackTrace();
		}
	}
}
